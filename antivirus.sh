#!/bin/bash

if [ "$#" -ne "1" ]; then
    echo "Usage: ./antivirus.sh <path>"
    exit
fi

find $1 -type f -exec bash -c '[[ $(file -b "'{}'") == *" executable "* ]] ' \; -print > files.data

IFS=$'\n'
for FILE in $(cat files.data)
do
    echo "File $FILE found..."
done

read -p "Do you want to remove those files? [Y/N]" OPTION

if [ "$OPTION" == "Y" ]; then
    IFS=$'\n'
    for FILE in $(cat files.data)
    do
        echo "Removing $FILE ..."
        rm $FILE
    done
fi

rm files.data

echo "Done!"
